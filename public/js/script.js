document.addEventListener("DOMContentLoaded", function() {

       document.getElementById('result').innerText='Message is:  ';
       document.getElementById('result').innerText += binaryToHex(document.getElementById('binary').innerText);
       if(document.getElementById('asString')){
           document.getElementById('asString').innerText = hex_to_ascii(binaryToHex(document.getElementById('binary').innerText));
       }
       var keysPlace = document.getElementById('keysplace');
       var keys = document.getElementsByClassName('keys');
       for(let i = 0;i<keys.length;i++){
           var temp = document.createElement("p"); 
           if(i<9){
               temp.innerHTML ='Key number  '+ (i+1)+' Hex: '+binaryToHex(keys[i].innerText) +' Binary: ' + keys[i].innerText;
           }else{
               temp.innerHTML ='Key number '+ (i+1)+' Hex: '+binaryToHex(keys[i].innerText) +' Binary: ' + keys[i].innerText;
           }
           keysPlace.appendChild(temp); 
       }

       var leftKeys = document.getElementsByClassName('leftk');
       var rightKeys = document.getElementsByClassName('rightk');
       for (let j = 0; j<leftKeys.length; j++){
           leftKeys[j].nextSibling.innerText += binaryToHex(leftKeys[j].innerText);
           rightKeys[j].nextSibling.innerText += binaryToHex(rightKeys[j].innerText);
       }       
});

function binaryToHex(s) {
    var i, k, part, accum, ret = '';
    for (i = s.length-1; i >= 3; i -= 4) {
        part = s.substr(i+1-4, 4);
        accum = 0;
        for (k = 0; k < 4; k += 1) {
            if (part[k] !== '0' && part[k] !== '1') {
                return { valid: false };
            }
            accum = accum * 2 + parseInt(part[k], 10);
        }
        if (accum >= 10) {
            ret = String.fromCharCode(accum - 10 + 'A'.charCodeAt(0)) + ret;
        } else {
            ret = String(accum) + ret;
        }
    }
    if (i >= 0) {
        accum = 0;
        for (k = 0; k <= i; k += 1) {
            if (s[k] !== '0' && s[k] !== '1') {
                return { valid: false };
            }
            accum = accum * 2 + parseInt(s[k], 10);
        }
        ret = String(accum) + ret;
    }
    return ret;
}

function hex_to_ascii(str1)
 {
    var hex  = str1.toString();
    var str = '';
    for (var n = 0; n < hex.length; n += 2) {
        str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
    }
    return str;
 }