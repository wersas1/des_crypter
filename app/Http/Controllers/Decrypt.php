<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Decrypt extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
       private static $shiftRightPattern = [0,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1];
		static function shiftRight(String $hkey) : Array{
		$hkey = str_split($hkey);
		$shiftedKeys = [];
		//array_push($shiftedKeys,implode('',$hkey));
		foreach (self::$shiftRightPattern as $shiftBy) {
			if($shiftBy === 1){
				array_unshift($hkey,array_pop($hkey));
			}
			if($shiftBy === 2){
				array_unshift($hkey,array_pop($hkey));
				array_unshift($hkey,array_pop($hkey));
			}
			array_push($shiftedKeys,implode('',$hkey));
		}
		
		return $shiftedKeys;
    }
}
