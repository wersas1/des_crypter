<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Convertable;
use App\Permutation;
use App\Message;
use App\Action;

class main extends Controller
{
	
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {	
	
	 $request->validate([
			'action'=>'required',
			'keyType'=>'required',
			'message'=>'required',
			'messageType'=>'required',
			'key'=>'required',
      ]);
	  
	
		$action = $request->get('action');
		$keyType = $request->get('keyType');
		$message = $request->get('message');
		$messageType = $request->get('messageType');
		$key = $request->get('key');
	
		echo "<script src='js/script.js'></script>";
		
		$typeOfKey = $keyType;
		$typeOfMessage = $messageType;
		
		if($typeOfMessage == 'string' && $action == 'decrypt'){
			echo "<p style='font-size:16px; color:red;'>Cant decrypt from plaintext</p>";
		}
		
		
		if($action =='decrypt'){
			echo "<p id='asString'></p>";
		}
		
		isset($message) ? $message = $_GET['message'] : $message = '';
		isset($message) ? $key = $_GET['key'] : $key = '';
		
		if($_GET['messageType'] !== 'string'){
			$message = str_replace(' ', '', $message);
			$message = preg_replace('/\s+/', '', $message);
		}
		if(isset($message) && isset($key)){
			$typeOfKey === 'hex' ? $binaryKey = Convertable::hexToBinary($key) : $binaryKey = Convertable::stringToBinary($key); 
		$firstKeyPermutation = Permutation::firstPermutation($binaryKey);
		$len = count(explode(' ', $firstKeyPermutation));
		$firstKeyHalf = implode('',array_slice(explode(' ', $firstKeyPermutation), 0, $len / 2));
		$secondKeyHalf = implode('',array_slice(explode(' ', $firstKeyPermutation), $len / 2));
		$leftKeys = Permutation::shiftLeft($firstKeyHalf);
		$rightKeys = Permutation::shiftLeft($secondKeyHalf);
		$concatenatedKeys = Permutation::concatenateKeys($leftKeys,$rightKeys);
		$finishedKeys = Permutation::secondPermutation($concatenatedKeys);
		$decryptionKeys = array_reverse($finishedKeys);
		$roundKeys = [];
		if($_GET["action"] == 'encrypt'){
			foreach ($finishedKeys as $key => $value) {
				$value=Message::bits2hex($value);
				//echo '<p style="display:block;" class="keys" id='.$key.'>'.$value.'</p>';
				$roundKeys[$key] = "Key[".$key."]: ".$value;
		}
		}else{
			foreach ($decryptionKeys as $key => $value) {
				$value=Message::bits2hex($value);
				//echo '<p style="display:block;" class="keys" id='.$key.'>'.$value.'</p>';
				$roundKeys[$key] = "Key[".$key."]: ".$value;
		}
		}
		if($_GET["action"] == 'encrypt'){
			$finishedKeys = $finishedKeys;
		}else{
			$finishedKeys = $decryptionKeys;
		}
		//----------------------------------------------------------------------------------------------------
		$messages = [];
		if(strlen($message) > 8){
			if($typeOfMessage !== 'hex'){
				$messageBlocks = Message::strMessageToBlock($message);
				foreach($messageBlocks as $block){
					array_push($messages,Convertable::stringToBinary($block));
				}
			}else{
				if(strlen($message) > 16){
					$messageBlocks = Message::hexMessageToBlock($message);
					foreach($messageBlocks as $block){
						array_push($messages,Convertable::hexToBinary($block));
					}
				} else {
					array_push($messages,Convertable::hexToBinary($message));
				}	
			}
		} else {
				$typeOfMessage === 'hex' ? array_push($messages,Convertable::hexToBinary($message)) : array_push($messages,Convertable::stringToBinary($message)); 
		}
		echo '<div id="keysplace"></div>';
		$finalBinaries = [];
		$xoredMessages = [];
		foreach($messages as $key=>$binaryMessage){
			$temp = $key+1;
			echo 'Message block number '.$temp.'<br>';
			$firstMessagePermutation = Message::firstPermutation($binaryMessage);
			$len = count(explode(' ', $firstMessagePermutation));
			$firstMessageHalf = implode('',array_slice(explode(' ', $firstMessagePermutation), 0, $len / 2));
			$secondMessageHalf = implode('',array_slice(explode(' ', $firstMessagePermutation), $len / 2));
			$eBit = Message::eBitSelection($secondMessageHalf);
			$xoredMessage = Message::calculateXOR($finishedKeys[0],$eBit);
			$xoredMessages[0]=Message::bits2hex($xoredMessage);
			$splitmessage = Message::splitArrayIntoSix($xoredMessage);
			$sBoxVals = Message::findTheVals($splitmessage);
			$pPermutationResult = Message::pPermutation($sBoxVals);
			$R1 = Message::calculateFinalXOR($firstMessageHalf,$pPermutationResult);
			$arrayRs = [$R1];
			$arrayLs = [$secondMessageHalf];
			$results = [];
			
				//echo "L0 ";
				//echo "<p class='leftk' style='display:inline; padding:0px; margin:0px;'>".$arrayLs[0].'</p><p id="" style="display:inline;">  Hex value:</p><br>';
				$resultsLeft[0] = "L0 ".Message::bits2hex($arrayLs[0]); 
				//echo "R0 ";
				//echo "<p class='rightk' style='display:inline; padding:0px; margin:0px;'>".$arrayRs[0].'</p><p id="" style="display:inline;">  Hex value:</p><br>';
				$resultsRight[0] = "R0 ".Message::bits2hex($arrayRs[0]);
				
			for($i = 1; $i < count($finishedKeys); $i++){
				$eBit = Message::eBitSelection($arrayRs[$i-1]);
				$xoredMessage = Message::calculateXOR($finishedKeys[$i],$eBit);
				$xoredMessages[$i]=Message::bits2hex($xoredMessage);
				$splitmessage = Message::splitArrayIntoSix($xoredMessage);
				$sBoxVals = Message::findTheVals($splitmessage);
				$pPermutationResult = Message::pPermutation($sBoxVals);
				$R = Message::calculateFinalXOR($arrayLs[$i-1],$pPermutationResult,true);
				array_push($arrayRs,$R);
				array_push($arrayLs,$arrayRs[$i-1]);
				//echo "L".$i.' ';
				//echo "<p class='leftk' style='display:inline; padding:0px; margin:0px;'>".$arrayLs[$i].'</p><p id="" style="display:inline;">  Hex value:</p><br>';
				$resultsLeft[$i] = "L".$i." ".Message::bits2hex($arrayLs[$i]); 
				//echo "R".$i.' ';
				//echo "<p class='rightk' style='display:inline; padding:0px; margin:0px;'>".$arrayRs[$i].'</p><p id="" style="display:inline;">  Hex value:</p><br>';
				//array_push($results,"R".[$i].$arrayRs[$i]);
				$resultsRight[$i] = "R".$i." ".Message::bits2hex($arrayRs[$i]);
			}
			$reversedBlocks = Message::reverseBlocks($arrayRs[count($arrayRs)-1],$arrayLs[count($arrayLs)-1]);
			$finalBinary = Message::finalPermutation($reversedBlocks);
			array_push($finalBinaries,$finalBinary);
		}
		$rework = '';
		foreach ($finalBinaries as $key => $value) {
			if($key === 0){
				$rework = $value;
				$rework=str_replace(" ","",$rework);
				$rework=Message::bits2hex($rework);
			}else if ($key === 1){
				$rework = Message::reverseBlocks($rework,$value);
				$rework=str_replace(" ","",$rework);
				$rework=Message::bits2hex($rework);
			}else if($key > 1){
				$rework=Message::reverseBlocks($rework,$value);
				$rework=str_replace(" ","",$rework);
				$rework=Message::bits2hex($rework);
			}
		}
		}
		//echo "<p style='display:none;' id='binary'>".str_replace(' ', '', $rework)."</p>";
		//echo '<div class="">&copy Vincentas Baubonis +37064711326</div>';
		
		return view('welcome',[        
			'resultsLeft' => $resultsLeft,
			'resultsRight' => $resultsRight,
			'roundKeys' => $roundKeys,
			'rework' => $rework,
			'xoredMessages' => $xoredMessages,
		]);
	}
}
