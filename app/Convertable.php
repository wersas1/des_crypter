<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Convertable extends Model
{
  private static $hexToBinDictionary =[
			  '0'=>'0000','1'=>'0001','2'=>'0010','3'=>'0011',
			  '4'=>'0100','5'=>'0101','6'=>'0110','7'=>'0111',
			  '8'=>'1000','9'=>'1001','A'=>'1010','B'=>'1011',
			  'C'=>'1100','D'=>'1101','E'=>'1110','F'=>'1111'
	];
	public static function hexToBinary(String $value) : String{
		$splitKey = str_split(strtoupper($value));
		$binaryValue = [];
		foreach ($splitKey as $key => $char){
			array_push($binaryValue,self::$hexToBinDictionary[$char]);
		}
		$binaryValue = implode('', $binaryValue);
		while(strlen($binaryValue) < 64){
			$binaryValue = $binaryValue.'0';
		}
		$binaryValue = chunk_split($binaryValue, 8, ' ');
		
		return trim($binaryValue);
	}
	/*
	* $string string to convert
	* $withSpace true if space after every 4th symbol is needed 
	*/
	public static function stringToBinary(String $string,bool $withSpace = true) : String
	{
	   $bin = array();
	    for($i=0; strlen($string)>$i; $i++){
	    	if(strlen(decbin(ord($string[$i]))) < 8){
	    		$temp = decbin(ord($string[$i]));
	    		while(strlen($temp) < 8){
	    			$temp = '0'.$temp;
	    		}
	    	array_push($bin, $temp);	
	    	}else{
	    		$bin[$i] = decbin(ord($string[$i]));
	    	}
	    }
	    while(count($bin) < 8){
	    	array_push($bin,'00000000');
	    }
	    return implode(' ',$bin);
	}
	public static function binaryToString(String $binary) : String
	{
	    $binaries = explode(' ', $binary);
	    $string = null;
	    foreach ($binaries as $binary) {
	        $string .= pack('H*', dechex(bindec($binary)));
	    }
	 
	    return $string;    
	}
	public static function binaryToHex(String $val) : String
	{
		$val = str_replace(' ', '', $val);
		return base_convert($val, 2, 16);
	}
}
