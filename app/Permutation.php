<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permutation extends Model
{
    	private static $firstPermutation = [
		57,49,41,33,25,17,9,
		1,58,50,42,34,26,18,
		10,2,59,51,43,35,27,
		19,11,3,60,52,44,36,
		63,55,47,39,31,23,15,
		7,62,54,46,38,30,22,
		14,6,61,53,45,37,29,
		21,13,5,28,20,12,4
	];
	private static $secondPermutation = [
		14,17,11,24,1,5,
		3,28,15,6,21,10,
		23,19,12,4,26,8,
		16,7,27,20,13,2,
		41,52,31,37,47,55,
		30,40,51,45,33,48,
		44,49,39,56,34,53,
		46,42,50,36,29,32
	];
	private static $shiftLeftPattern = [1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1];
	static function firstPermutation(String $bkey) : String{
		$bkey = str_replace(' ', '', $bkey);
		$bkey = str_split($bkey);
		$newArr = [];
		foreach (self::$firstPermutation as $key => $value) {
			$newArr[$key] = $bkey[$value-1];
		}
		$newArr = implode('',$newArr);
		$newArr = chunk_split($newArr, 7, ' ');
		return trim($newArr);
	}
	static function shiftLeft(String $hkey) : Array{
		$hkey = str_split($hkey);
		$shiftedKeys = [];
		//array_push($shiftedKeys,implode('',$hkey));
		foreach (self::$shiftLeftPattern as $shiftBy) {
			if($shiftBy === 1){
				array_push($hkey,array_shift($hkey));
			}
			if($shiftBy === 2){
				array_push($hkey,array_shift($hkey));
				array_push($hkey,array_shift($hkey));
			}
			array_push($shiftedKeys,implode('',$hkey));
		}
		
		return $shiftedKeys;
	}
	static function concatenateKeys(Array $firstKey,Array $secondKey) : Array{
		$resultArray = [];
		for($i = 0; $i < 16; $i++){
			$firstKey[$i] = str_split($firstKey[$i]);
			$secondKey[$i] = str_split($secondKey[$i]);
			$concatenatedArray = array_merge($firstKey[$i],$secondKey[$i]);
			array_push($resultArray,implode('',$concatenatedArray));
		}
		
		return $resultArray;
	}
	static function secondPermutation(Array $concatenatedKeys) : Array{
		$permutedArray = [];
		foreach($concatenatedKeys as $ckeys){
			$ckeys = str_split($ckeys);
			$newArr = [];
			foreach(self::$secondPermutation as $key => $value){
				$newArr[$key] = $ckeys[$value-1];
			}
			$newArr = implode('',$newArr);
			array_push($permutedArray, $newArr);
		}
	
		return $permutedArray;
	}
}
