<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	private static $firstPermutation = [
	 58,50,42,34,26,18,10,2,
	 60,52,44,36,28,20,12,4,
	 62,54,46,38,30,22,14,6,
	 64,56,48,40,32,24,16,8,
	 57,49,41,33,25,17,9,1,
	 59,51,43,35,27,19,11,3,
	 61,53,45,37,29,21,13,5,
	 63,55,47,39,31,23,15,7
	];
	private static $eTable = [
	 32, 1, 2, 3, 4, 5,
	 4, 5, 6, 7, 8, 9,
	 8, 9, 10, 11, 12, 13,
	 12, 13, 14, 15, 16, 17,
	 16, 17, 18, 19, 20, 21,
	 20, 21, 22, 23, 24, 25,
	 24, 25, 26, 27, 28, 29,
	 28, 29, 30, 31, 32, 1
	];
	private static $pPermutation = [
	 16, 7, 20, 21,
	 29, 12, 28, 17,
	 1, 15, 23, 26,
	 5, 18, 31, 10,
	 2, 8,24, 14,
	 32, 27, 3, 9,
	 19, 13, 30, 6,
	 22, 11, 4, 25 
	];
	private static $finalPermutation = [
	 40, 8, 48, 16, 56, 24, 64, 32,
	 39, 7, 47, 15, 55, 23, 63, 31,
	 38, 6, 46, 14, 54, 22, 62, 30,
	 37, 5, 45, 13, 53, 21, 61, 29,
	 36, 4, 44, 12, 52, 20, 60, 28,
	 35, 3, 43, 11, 51, 19, 59, 27,
	 34, 2, 42, 10, 50, 18, 58, 26,
	 33, 1, 41, 9,49, 17, 57, 25 
	];
	private static $allArr = array();
	static function firstPermutation(String $bkey) : String{
		$bkey = str_replace(' ', '', $bkey);
		$bkey = str_split($bkey);
		$newArr = [];
		foreach (self::$firstPermutation as $key => $value) {
			$newArr[$key] = $bkey[$value-1];
		}
		$newArr = implode('',$newArr);
		$newArr = chunk_split($newArr, 8, ' ');
		return trim($newArr);
	}
	static function eBitSelection(String $rkey)  : String{
		$rkey = str_replace(' ', '', $rkey);
		$rkey = str_split($rkey);
		$newArr = [];
		foreach (self::$eTable as $key => $value) {
			$newArr[$key] = $rkey[$value-1];
		}
		$newArr = implode('',$newArr);
		$newArr = chunk_split($newArr, 6, ' ');
		return trim($newArr);
	}
	static function calculateXOR(String $cKey,String $bitSel)  : String{
		$xor1 = gmp_init($cKey, 2);
		$xor2 = gmp_init($bitSel, 2);
		$xor3 = gmp_xor($xor1, $xor2);
			if(strlen(gmp_strval($xor3, 2)) < 48){
				$temp = gmp_strval($xor3, 2);
				while(strlen($temp) < 48){
					$temp = '0'.$temp;
				}
				return $temp;
			}
		return gmp_strval($xor3, 2);		
	}
	static function calculateFinalXOR(String $cKey,String $bitSel)  : String{
		$xor1 = gmp_init($cKey, 2);
		$xor2 = gmp_init($bitSel, 2);
		$xor3 = gmp_xor($xor1, $xor2);
		if(strlen(gmp_strval($xor3, 2)) < 32){
				$temp = gmp_strval($xor3, 2);
				while(strlen($temp) < 32){
					$temp = '0'.$temp;
				}
				return $temp;
			}	
		return gmp_strval($xor3, 2);		
	}
	static function splitArrayIntoSix(String $msg) : Array{
		$msg = str_split($msg);
		$returnedArr = [];
		$resultArr = array_chunk($msg, 6);
		foreach($resultArr as $key => $res){
			$returnedArr[$key] = implode('', $res);
		}
		return ($returnedArr);
	}
	static function findTheVals(Array $splitMessage) : String{
		$s1 = array (
		array(14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7),
		array(0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8),
		array(4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0),
		array(15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13 ));
		$s2 = array (
		array(15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10),
		array(3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5),
		array(0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15),
		array(13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9 ));
		$s3 = array (
		array(10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8),
		array(13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1),
		array(13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7),
		array(1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12 ));
		$s4 = array (
		array(7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15),
		array(13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9),
		array(10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4),
		array(3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14 ));
		$s5 = array (
		array(2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9),
		array(14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6),
		array(4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14),
		array(11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3 ));
		$s6 = array (
		array(12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11),
		array(10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8),
		array(9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6),
		array(4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13 ));
		$s7 = array (
		array(4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1),
		array(13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6),
		array(1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2),
		array(6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12 ));
		$s8 = array (
		array(13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7),
		array(1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2),
		array(7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8),
		array(2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11 ));
		array_push(self::$allArr,$s1);
		array_push(self::$allArr,$s2);
		array_push(self::$allArr,$s3);
		array_push(self::$allArr,$s4);
		array_push(self::$allArr,$s5);
		array_push(self::$allArr,$s6);
		array_push(self::$allArr,$s7);
		array_push(self::$allArr,$s8);
		$resArr = [];
		foreach($splitMessage as $key=>$message){
			$rowPos = $message[0].$message[5];
			$colPos = $message[1].$message[2].$message[3].$message[4];
			$found = decbin(self::$allArr[$key][bindec($rowPos)][bindec($colPos)]);
			if(strlen($found) !== 4){
				while (strlen($found) !== 4){
					$found = '0'.$found;
				}
			}
			array_push($resArr,$found);
		}
		return implode('', $resArr);
	}
	static function pPermutation(String $sBoxVals) : String{
		$sBoxVals = str_split($sBoxVals);
		$newArr = [];
		foreach (self::$pPermutation as $key => $value) {
			$newArr[$key] = $sBoxVals[$value-1];
		}
		$newArr = implode('',$newArr);
		$newArr = chunk_split($newArr, 4, ' ');
		return trim($newArr);
	}
	static function reverseBlocks($firstBlock,$secondBlock) : String{
		$firstBlock = str_split($firstBlock);
		$secondBlock = str_split($secondBlock);
		return implode('',array_merge($firstBlock,$secondBlock));
	}
	static function finalPermutation(String $revBlock) : String{
		$revBlock = str_replace(' ', '', $revBlock);
		$revBlock = str_split($revBlock);
		$newArr = [];
		foreach (self::$finalPermutation as $key => $value) {
			$newArr[$key] = $revBlock[$value-1];
		}
		$newArr = implode('',$newArr);
		$newArr = chunk_split($newArr, 8, ' ');
		return trim($newArr);
	}
	static function strMessageToBlock(String $message){
		if(strlen($message) <= 8){
		 	return $message;
		}else{
			$message = str_split($message);
			$blocks = array_chunk($message, 8);
			foreach ($blocks as $key => $value) {
				// while(count($value) < 8){
				// 	array_push($value,'0');
				// }
				$blocks[$key] = implode('',$value);
			}
			return $blocks;	
		}
	}
		static function hexMessageToBlock(String $message){
		if(strlen($message) <= 16){
		 	return $message;
		}else{
			$message = str_split($message);
			$blocks = array_chunk($message, 16);
			foreach ($blocks as $key => $value) {
				// while(count($value) < 8){
				// 	array_push($value,'0');
				// }
				$blocks[$key] = implode('',$value);
			}
			return $blocks;	
		}
	}
	
	public static function bits2hex($bin)
	{
	   $out = "";
	   for($i=0;$i<strlen($bin);$i+=8)
	   {
		  $byte = substr($bin,$i,8); if( strlen($byte)<8 ) $byte .= str_repeat('0',8-strlen($byte));
		  $out .= base_convert($byte,2,16);
	   }
	   return $out;
	}

}